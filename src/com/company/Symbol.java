package com.company;

public class Symbol {

    public Symbol() {
        sectionOne();
        getSymbolA_Z();
        getSymbol_z_a();
        getSymbolRu();
        getSymbolNum();
        getASCII();
    }

    public void sectionOne() {
        System.out.println("Section 1");
    }

    public String getSymbolA_Z() {
        String result = "";
        for (char c = 'A'; c <= 'Z'; c++) {
            result += c + " ";
        }
        System.out.println("(1.1) Output symbol A-Z: " + result);
        return result;
    }

    public String getSymbol_z_a() {
        String result = "";
        for (char c = 'z'; c >= 'a'; c--) {
            result += c + " ";
        }
        System.out.println("(1.2) Output symbol z-a: " + result);
        return result;
    }

    public String getSymbolRu() {
        String result = "";
        for (char c = 'а'; c <= 'я'; c++) {
            result += c + " ";
        }
        System.out.println("(1.3) Output symbol а-я: " + result);
        return result;
    }

    public String getSymbolNum() {
        String result = "";
        for (char c = '0'; c <= '9'; c++) {
            result += c + " ";
        }
        System.out.println("(1.4) Output symbol number: " + result);
        return result;
    }

    public String getASCII() {
        String result = "";
        for (char c = '!'; c <= '~'; c++) {
            result += c + " ";
        }
        System.out.println("(1.5) Output symbol ASCII: " + result);
        return result;
    }


}
