package com.company;

public class Strings {
    public Strings() {
        sectionThree();
        minLengthWord();
        replaceLastThree();
        backspace();
        noRepeatSymbol();
        getInputQuanWord();
        deletePosition();
        reverseLine();
        lastWordDelete();
    }

    public void sectionThree() {
        System.out.println("\nSection 3");
    }

    public int minLengthWord() {
        String words = "Hi, I'm Anton, and I'm taking course at DevEducation.";
        String[] splite = words.split(" ");
        String ch1 = splite[0];
        for (int i = 1; i < splite.length; i++) {
            if (ch1.length() > splite[i].length())
                ch1 = splite[i];
        }
        System.out.printf("(3.1) Length min word: %d, word: '%s'\n", ch1.length(), ch1);
        return ch1.length();
    }

    public String replaceLastThree() {
        String[] words = {"Hello", "world", "my", "own", "Chinese", "assembly"};
        StringBuilder myName = null;
        for (int i = 0; words.length > i; i++) {
            if (words[i].length() > 3) {
                myName = new StringBuilder(String.valueOf(words[i]));
                myName.setCharAt(words[i].length() - 3, '$');
                myName.setCharAt(words[i].length() - 2, '$');
                myName.setCharAt(words[i].length() - 1, '$');
                System.out.println("(3.2) Replace last three: " + myName);
            }
        }
        return myName.toString();
    }

    public String backspace() {
        String backspaceCheck = "Hello,I write this at night,and to be honest,I want to sleep.";
        for (int i = 0; i < backspaceCheck.length() - 1; i++) {
            char c = backspaceCheck.charAt(i);
            if (backspaceCheck.charAt(i + 1) != ' ') {
                if ((c < '0' && c >= '!') || (c < 'A' && c > '9')) {
                    backspaceCheck = backspaceCheck.replace(Character.toString(c), (c + " "));
                }
            }
        }
        System.out.println("(3.3) NoRepeat symbols: " + backspaceCheck);
        return backspaceCheck;
    }

    public String noRepeatSymbol() {
        String str = "Hello,I write this at night,and to be honest,I want to sleep.";
        StringBuilder sb = new StringBuilder();
        int idx;
        for (int i = 0; i < str.length(); i++) {
            char c = str.charAt(i);
            idx = str.indexOf(c, i + 1);
            if (idx == -1) {
                sb.append(c);
            }
        }
        System.out.println("(3.4) NoRepeat symbols: " + sb);
        return sb.toString();
    }

    public int getInputQuanWord() {
        String str = "OneWord TwoWord ThreeWord";
        int count = 0;

        if (str.length() != 0) {
            count++;
            for (int i = 0; i < str.length(); i++) {
                if (str.charAt(i) == ' ') {
                    count++;
                }
            }
        }
        System.out.println("(3.5) Quantity word: " + count);
        return count;
    }

    public String deletePosition() {
        String text = "Hello, I write this at night, and to be honest, I want to sleep.";
        String newText = text.replace("I write this at night, ", "");
        System.out.println("(3.6) Delete position: " + newText);
        return newText;
    }

    public String reverseLine() {
        String text = "Hello,I write this at night,and to be honest,I want to sleep.";
        StringBuffer buffer = new StringBuffer(text);
        buffer.reverse();
        System.out.println("(3.7) Reverse text: " + buffer);
        return buffer.toString();
    }

    public String lastWordDelete() {
        System.out.print("(3.8) Last word delete: ");
        String text = "Hello, I write this at night, and to be honest, I want to sleep.";
        String lol = text.substring(0, text.lastIndexOf(" "));
        System.out.println(lol);
        return lol;
    }
}
