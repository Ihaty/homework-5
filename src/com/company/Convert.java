package com.company;

public class Convert {
    public Convert() {
        sectionTwo();
        covertIntToStr();
        convertDoubToStr();
        convertStrToInt();
        convertStrToDoub();
    }

    public void sectionTwo() {
        System.out.print("\nSection 2");
    }

    public String covertIntToStr() {
        int num = 123;
        String transform = Integer.toString(num);
        System.out.println("\n(2.1) Transform Integer to String: " + transform);
        return transform;
    }

    public String convertDoubToStr() {
        double num = 1.02;
        double num1 = 1.04;
        String transform = Double.toString(num + num1);
        System.out.println("(2.2) Real number: "  + transform);
        return transform;
    }

    public int convertStrToInt() {
        System.out.print("(2.3) Transform String to Int number: ");
        String num = "1";
        String num2 = "2";
        int transform = Integer.parseInt(num);
        int transform2 = Integer.parseInt(num2);
        System.out.println(transform + transform2);
        return transform + transform2;
    }

    public double convertStrToDoub() {
        System.out.print("(2.4) Transform String real number to Int: ");
        String num = "1.02";
        String num2 = "2.03";
        double transform = Double.valueOf(num);
        double transform2 = Double.valueOf(num2);
        System.out.println(transform + transform2);
        return transform + transform2;
    }
}
