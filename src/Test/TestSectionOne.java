package Test;

import com.company.Symbol;
import org.junit.jupiter.api.BeforeAll;
import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.*;

public class TestSectionOne {


    private static Symbol s1;

    @BeforeAll
    static void setSectionOne() {
        s1 = new Symbol();
    }

    @Test
    void getSymbolA_Z() {
        String expected = "A B C D E F G H I J K L M N O P Q R S T U V W X Y Z ";
        String actual = s1.getSymbolA_Z();
        assertEquals(expected, actual);
    }

    @Test
    void getSymbol_z_a() {
        String expected = "z y x w v u t s r q p o n m l k j i h g f e d c b a ";
        String actual = s1.getSymbol_z_a();
        assertEquals(expected, actual);
    }

    @Test
    void getSymbolRu() {
        String expected = "а б в г д е ж з и й к л м н о п р с т у ф х ц ч ш щ ъ ы ь э ю я ";
        String actual = s1.getSymbolRu();
        assertEquals(expected, actual);
    }

    @Test
    void getSymbolNum() {
        String expected = "0 1 2 3 4 5 6 7 8 9 ";
        String actual = s1.getSymbolNum();
        assertEquals(expected, actual);
    }

    @Test
    void getASCII() {
        String expected = "! \" # $ % & ' ( ) * + , - . / 0 1 2 3 4 5 6 7 8 9 : ; < = > ? @ A B C D E F G H I J K L M N O P Q R S T U V W X Y Z [ \\ ] ^ _ ` a b c d e f g h i j k l m n o p q r s t u v w x y z { | } ~";
        String actual = s1.getASCII();
        assertNotSame(expected, actual);
    }
}

