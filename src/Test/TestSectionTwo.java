package Test;

import com.company.Convert;
import org.junit.jupiter.api.BeforeAll;
import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.assertEquals;

public class TestSectionTwo {


    private static Convert s2;

    @BeforeAll
    static void setSectionTwo() {
        s2 = new Convert();
    }

    @Test
    void covertIntToStr() {
        String expected = "123";
        String actual = s2.covertIntToStr();
        assertEquals(expected, actual);
    }

    @Test
    void convertDoubToStr() {
        String expected = "2.06";
        String actual = s2.convertDoubToStr();
        assertEquals(expected, actual);
    }

    @Test
    void convertStrToInt() {
        int expected = 3;
        int actual = s2.convertStrToInt();
        assertEquals(expected, actual);
    }

    @Test
    void convertStrToDoub() {
        double expected = 3.05;
        double actual = s2.convertStrToDoub();
        assertEquals(expected, actual);
    }
}

