package Test;

import com.company.Strings;
import org.junit.jupiter.api.BeforeAll;
import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.assertEquals;

class TestSectionTree {
    private static Strings s3;

    @BeforeAll
    static void setB3() {
        s3 = new Strings();
    }

    @Test
    void minLengthWord() {
        int expected = 2;
        int actual = s3.minLengthWord();
        assertEquals(expected, actual);
    }

    @Test
    void replaceLastThree() {
        String expected = "assem$$$";
        String actual = s3.replaceLastThree();
        assertEquals(expected, actual);
    }

    @Test
    void backspace() {
        String expected = "Hello, I write this at night, and to be honest, I want to sleep.";
        String actual = s3.backspace();
        assertEquals(expected, actual);
    }

    @Test
    void noRepeatSymbol() {
        String expected = "Hrigdbh,Iwanto slep.";
        String actual = s3.noRepeatSymbol();
        assertEquals(expected, actual);
    }

    @Test
    void getInputQuanWord() {
        int expected = 3;
        int actual = s3.getInputQuanWord();
        assertEquals(expected, actual);
    }

    @Test
    void deletePosition() {
        String expected = "Hello, and to be honest, I want to sleep.";
        String actual = s3.deletePosition();
        assertEquals(expected, actual);
    }

    @Test
    void reverseLine() {
        String expected = ".peels ot tnaw I,tsenoh eb ot dna,thgin ta siht etirw I,olleH";
        String actual = s3.reverseLine();
        assertEquals(expected, actual);
    }

    @Test
    void lastWordDelete() {
        String expected = "Hello, I write this at night, and to be honest, I want to";
        String actual = s3.lastWordDelete();
        assertEquals(expected, actual);
    }
}